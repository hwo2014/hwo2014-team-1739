import pprint

# ======================
# ===== Car Info =====
# ======================
class CarInfo(object):
    
    def __init__(self, carInfo):
        print("Creating Car Info...")
        
        self.carInfo      = carInfo
        self.name         = carInfo["id"]["name"]
        self.color        = carInfo["id"]["color"]
        self.length       = carInfo["dimensions"]["length"]
        self.width        = carInfo["dimensions"]["width"]
        self.guideFlagPos = carInfo["dimensions"]["guideFlagPosition"]
        
        print("Car Info Complete!")
        
    def dump(self):
        pprint.pprint(self.carInfo)
