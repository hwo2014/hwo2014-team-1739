import json
import socket
import sys
    
# Internal Imports
import quali_ai
import race_ai

# ================
# ===== Main =====
# ================
if __name__ == "__main__":
    if len(sys.argv) != 7:
        print("Usage: ./run host port trackname carcount botname botkey")
    else:
        host, port, trackname, carcount, name, key = sys.argv[1:7]

        print("Connecting with parameters:")
        print("host={0}, port={1}, trackname={2}, carcount={3}, bot name={4}, key={5}".format(*sys.argv[1:7]))

        # connect to server
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        
        # Qualifier AI
        quali_bot = quali_ai.QualiBot(s, name, key, trackname, carcount)
        
        try:
            quali_bot.run()
        except StopIteration:
            race_bot = race_ai.RaceBot(s, name, key, trackname, carcount)

            race_bot.deceleration_exponent = quali_bot.deceleration_exponent

            print race_bot.deceleration_exponent
            
            race_bot.turn_friction = quali_bot.turn_friction
            race_bot.max_slip = quali_bot.max_slip
            race_bot.lap = quali_bot.lap
            race_bot.track = quali_bot.track
            race_bot.gps = quali_bot.gps

            race_bot.currPos = quali_bot.currPos
            race_bot.prevPos = quali_bot.prevPos
            race_bot.firstPos = quali_bot.firstPos

            race_bot.run()
