import json
import socket
import sys
import math
import inspect
import pprint
import traceback

import sys

import track_info
import car_info
import gps


# =========================
# ===== Qualifier Bot =====
# =========================
class QualiBot(object):

    def __init__(self, socket, name, key, trackname, carcount):
        self.socket       = socket
        self.name         = name
        self.key          = key
        self.trackname    = trackname
        self.carcount     = carcount
        self.track_pieces = None
        self.positions    = []

        self.tick         = 0
        self.lap          = 1

        # variables to contain positional history
        self.prevPos      = None
        self.currPos      = None
        self.firstPos     = None

        self.prevAngle    = None
        self.currAngle    = None

        self.prevAngleSpeed = None
        self.currAngleSpeed = None

        #v(t) = v(0) * exp(deceleration_exponent * t)
        self.deceleration_exponent = 0
        self.turn_friction         = 0
        self.max_slip              = 60 # may be changed, seems to be constant for now

        self.turbo_available       = False
        self.turboing              = 0
        self.turbo_duration_msec   = 0
        self.turbo_duration_ticks  = 0
        self.turbo_factor          = 0

        #v(t) = acceleration_constant * (1 - exp(-acceleration_exponent * t))
        #self.acceleration_exponent = 0.01
        #self.acceleration_constant = 6.0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("joinRace", { "botId" : {"name": self.name, "key": self.key},
                                      "trackName" : self.trackname,
                                      "carCount" : self.carcount
                                     })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def switch_lanes(self, direction):
        self.msg("switchLane", direction)

    def turbo(self, turbo_message):
        print("ZOOM !! Turbo Tahme !!")
        self.msg("turbo", turbo_message)

    def run(self):
        self.join()
        self.msg_loop()

    def update_deceleration(self):
        v_0 = 0.5 * self.track.dist_between(self.positions[6], self.positions[8])
        v_1 = 0.5 * self.track.dist_between(self.positions[7], self.positions[9])

        self.deceleration_exponent = -math.log(v_0 / v_1)

    def caculate_turn_friction(self, me, data, game_tick):
        radius = self.track.pieces[me['piecePosition']['pieceIndex']].get('radius')
        if radius:
            if me['angle']:
                speed = self.gps.get_current_speed(self.prevPos["piecePosition"], self.currPos["piecePosition"], 1)
                self.turn_friction = speed ** 2 / radius
                self.throttle(0.5)
                print( "turn friction: " + str( self.turn_friction ))
            else:
                self.throttle(1.0)
        else:
            self.throttle(0.5)

    def calculate_braking(self, me, data, game_tick):
        if self.lap == 1:
            if self.on_start_finish_straight():
                #go quickly on what's normally the longest straight
                self.throttle(1.0)
            else:
                self.throttle(0.3) 

        if self.lap == 2:
            self.throttle(0.0)
            if len(self.positions) < 10:
                self.positions.append(me['piecePosition'])

            if len(self.positions) == 10:
                #have enough data points, figure out the deceleration
                self.update_deceleration()
                print( "Deceleration Exponent: " + str( self.deceleration_exponent ))

    # ---------------------
    # ----- Callbacks -----
    # ---------------------
    def on_join(self, data):
        print("Joined")

    def on_game_init(self, data):
        print("On game init...")
        
        # Setup track information
        self.track = track_info.TrackInfo( data["race"] )
        self.track.dump()
        
        # Setup car information
        self.car = car_info.CarInfo( self.track.get_car_info( self.name ) )

        # Setup GPS
        self.gps = gps.Gps( self.track )
        

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_turbo_start(self, data):
        self.turboing = True

    def on_turbo_end(self, data):
        self.turboing = False
    
    def on_start_finish_straight(self):
        start_pos = {'pieceIndex': 0, 'inPieceDistance': 0, 'lane': self.currPos['piecePosition']['lane']}
        dist_to_start = self.track.dist_between(self.currPos["piecePosition"], start_pos)

        if dist_to_start <= self.track.dist_from_last_curve_to_start():
            return True
        else:
            return False

    def on_car_positions(self, data, game_tick = None):

        # increment tick
        self.tick += 1

        for car in data:
            if car['id']['name'] == self.name:
                me = car

        # save current and previous positions
        if self.currPos is None:
            self.currPos = me
            self.firstPos = me
        else:
            self.prevPos = self.currPos
            self.currPos = me

        # populate angles
        if self.currAngle is None:
            self.currAngle = me['angle']
        else:
            self.prevAngle = self.currAngle
            self.currAngle = me['angle']

        if game_tick is None:
            #data is set up, shouldn't respond with a message so just exit
            return

        # populate angle speeds
        #if self.prevAngle is not None and self.currAngle is not None:
        #    if self.currAngleSpeed is None:
        #        self.currAngleSpeed = self.currAngle - self.prevAngle
        #    else:
        #        self.prevAngleSpeed = self.currAngleSpeed
        #        self.currAngle = self.currAngle - self.prevAngle

        #if self.prevPos is not None and self.currPos is not None:
        #    if self.gps.get_current_speed( self.prevPos["piecePosition"], self.currPos["piecePosition"], 1) >= 5.5:
        #        self.throttle(0.55)
        #    else:
        #        self.throttle(1.0)

        # ---- Test current speed
        #if self.prevPos is not None and self.currPos is not None:
        #    print( " Tick: " + str(self.tick) +
        #           " | Piece: " + str(me['piecePosition']['pieceIndex'])# + 
                   #" | Angle: " + str(car["angle"]) +
                   #" | Speed: " + str(self.gps.get_current_speed( self.prevPos["piecePosition"],
                   #                                               self.currPos["piecePosition"], 1)) +
                   #" | Angle Change Speed: " + str( self.gps.get_current_angle_change(self.prevAngle, self.currAngle)))
        #        )

        # ----- Determine Constants -----
        if self.turn_friction == 0:
            return self.caculate_turn_friction(me, data, game_tick)
        elif self.deceleration_exponent == 0:
            return self.calculate_braking(me, data, game_tick)
        #----- Run Real Bot -----
        else:
            raise StopIteration("got all necessary parameters")
        

    def on_crash(self, data):
        print("** Crashed ** | " + data['name'])

    def on_game_end(self, data):
        print("Race ended")

    def on_error(self, data):
        print("Error: {0}".format(data))

    def on_turbo_available(self, data):
        print ("Turbo Is Now Available!")
        pprint.pprint( data )

        self.turbo_available = True

        self.turbo_duration_msec   = data['turboDurationMilliseconds']
        self.turbo_duration_ticks  = data['turboDurationTicks']
        self.turbo_factor          = data['turboFactor']

    def on_lap_finished(self, data):
        print("Lap " + str(self.lap) + " Finished in time " + str(data['lapTime']['millis']))
        if data['car']['name'] == self.name:
            self.lap += 1

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'lapFinished': self.on_lap_finished,
            'turboAvailable': self.on_turbo_available,
            'gameInit': self.on_game_init,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                if msg_type == 'carPositions' and msg.get('gameTick'):
                    msg_map[msg_type](data, msg['gameTick'])
                else:
                    msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
            line = socket_file.readline()
