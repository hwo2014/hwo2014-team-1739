import pprint
import math

# ======================
# ===== Track Info =====
# ======================
class TrackInfo(object):
    
    # Initialize the data
    def __init__(self, raceData):
        print("Setting Track Data")
        
        self.fullData      = raceData
        self.id            = raceData["track"]["id"]
        self.name          = raceData["track"]["name"]
        self.pieces        = raceData["track"]["pieces"]
        self.lanes         = raceData["track"]["lanes"]
        self.startingPoint = raceData["track"]["startingPoint"]
        self.cars          = raceData["cars"]
        self.laps          = raceData["raceSession"]["laps"]
        self.maxLapTime    = raceData["raceSession"]["maxLapTimeMs"]
        self.quickRace     = raceData["raceSession"]["quickRace"]

    # Dump Information (pretty) 
    def dump(self):
        pprint.pprint(self.fullData)
        
    # Get Car Info
    #   Gets the car information given the name of the car
    def get_car_info(self, name):
        for index in range(len(self.cars)):
            if self.cars[index]["id"]["name"] == name:
                return self.cars[index]

    # length of arc
    def get_length_of_arc(self, angle, radius):
        return (angle/360) * 2 * math.pi * radius

    # current lane distance from center
    def get_lane_dist_from_center(self, laneIndex):
        return self.lanes[laneIndex]["distanceFromCenter"]

    # get real turn radius
    def get_real_turn_radius(self, turnAngle, radius, laneIndex):

        # if right turn...
        # 1) if in left lane ADD to radius
        # 2) if in right lane SUB from radius
        if turnAngle >= 0:
            return radius - self.get_lane_dist_from_center(laneIndex)
        else:
            return radius + self.get_lane_dist_from_center(laneIndex)

    # Distance to next turn
    #   given the current position - give the length to the next *change of radius*
    #   returns -1 if an error occurs
    def dist_and_radius_of_next_turn(self, position):

        index     = position["pieceIndex"]
        laneIndex = position["lane"]["startLaneIndex"]

        # -- Calculate distance to end of piece first
        # if currently on a straight
        if "length" in self.pieces[index]:
            distance = self.pieces[index]["length"] - position["inPieceDistance"]
        # if currently on a curve
        else:
            currentTurnAngle  = self.pieces[index]["angle"]
            currentTurnRadius = self.get_real_turn_radius( currentTurnAngle,
                                                           self.pieces[index]["radius"],
                                                           laneIndex )

            distance = abs( self.get_length_of_arc( currentTurnAngle,
                                                    currentTurnRadius )) - position["inPieceDistance"]

        # -- Add pieces to distance
        while True:

            # if starting index is the "end piece index + 1" (wrap around)
            if index == len(self.pieces):
                index = 0

            nextIndex = index + 1
            if nextIndex == len(self.pieces):
                nextIndex = 0

            nextPiece = self.pieces[nextIndex]

            # if next piece is a curve
            if "radius" in nextPiece:

                if "length" in self.pieces[index]:
                    return distance, nextPiece["radius"]
                else:
                    if self.pieces[index]["radius"] != nextPiece["radius"]:
                        return distance, nextPiece["radius"]
                    else:
                        turnAngle  = nextPiece["angle"]
                        turnRadius = self.get_real_turn_radius( turnAngle,
                                                                nextPiece["radius"],
                                                                laneIndex )

                        distance += abs( self.get_length_of_arc( turnAngle, turnRadius) )
            # if next piece is a straight
            else:
                distance += nextPiece["length"]
            
            index += 1

    # Distance between two positions
    def dist_between(self, startPos, endPos):

        index     = startPos["pieceIndex"]
        laneIndex = startPos["lane"]["startLaneIndex"]

        # -- Catch case in the same peice
        if index == endPos["pieceIndex"]:
            return endPos["inPieceDistance"] - startPos["inPieceDistance"]

        # -- Calculate distance to end of piece first
        if "length" in self.pieces[index]:
            distance = self.pieces[index]["length"] - startPos["inPieceDistance"]
        else:
            currentTurnAngle  = self.pieces[index]["angle"]
            currentTurnRadius = self.get_real_turn_radius( currentTurnAngle,
                                                           self.pieces[index]["radius"],
                                                           laneIndex )

            distance = abs( self.get_length_of_arc( currentTurnAngle,
                                                    currentTurnRadius )) - startPos["inPieceDistance"]

        # -- Add pieces to distance
        while True:
            
            # if starting index is the "end piece index + 1" (wrap around)
            if index == len(self.pieces):
                index = 0

            nextIndex = index + 1
            if nextIndex == len(self.pieces):
                nextIndex = 0

            nextPiece = self.pieces[nextIndex]

            if nextIndex == endPos["pieceIndex"]:
                return distance + endPos["inPieceDistance"]
            else:
                if "length" in self.pieces[nextIndex]:
                    distance += nextPiece["length"]
                else:
                    turnAngle  = nextPiece["angle"]
                    turnRadius = self.get_real_turn_radius( turnAngle,
                                                            nextPiece["radius"],
                                                            laneIndex )

                    distance += abs( self.get_length_of_arc( turnAngle, turnRadius) )
            
            index += 1

    def dist_from_last_curve_to_start(self):

        index = len(self.pieces) - 1
        distance = 0

        while "length" in self.pieces[index]:
            distance += self.pieces[index]["length"]
            index -= 1

        return distance

    def on_curve(self, currPos):
        if "length" in self.pieces[ currPos["pieceIndex"] ]:
            return False
        else:
            return True

    def on_switch(self, currPos):
        if "switch" in self.pieces[ currPos["pieceIndex"] ]:
            return True
        else:
            return False

    # Look for the next turn
    #   used for switch lane logic
    def dir_of_next_turn_piece(self, currPos):

        index = currPos['piecePosition']["pieceIndex"]
        switch_found = False;

        while True:

            if index == len(self.pieces):
                index = 0

            # walk through the thing until u hit a switch
            if 'switch' in self.pieces[index]:
                switch_found = True

            if switch_found == False:
                index += 1
                continue
            else:
                # if it's a straight, skip
                if "length" in self.pieces[index]:
                    index += 1
                else:
                    if self.pieces[index]['angle'] < 0:
                        return -1
                    else:
                        return 1

    # get the length of the next straight
    #   used mainly for the turbo decision code
    def get_length_of_next_straight(self, currPos):

        index = currPos['piecePosition']["pieceIndex"]
        if "radius" in self.pieces[index]:
            return         

        distance = self.pieces[index]["length"] - currPos['piecePosition']["inPieceDistance"]

        while True:

            if index == len(self.pieces):
                index = 0

            # if it's a straight, skip
            if "length" in self.pieces[index]:
                distance += self.pieces[index]['length']
                index += 1
            else:
                return distance










