import quali_ai
import track_info
import gps
import math

import sys

class RaceBot(quali_ai.QualiBot): # most function responses will be the same, overriden as needed

    def __init__(self, socket, name, key, trackname, carcount):
        self.socket       = socket
        self.name         = name
        self.key          = key
        self.trackname    = trackname
        self.carcount     = carcount
        self.track        = None
        self.gps          = None

        self.deceleration_exponent = 0
        self.turn_friction         = 0
        self.max_slip              = 60 # may be changed, seems to be constant for now

        self.prevPos         = None
        self.currPos         = None
        self.lap             = 0
        self.turbo_available = False

        self.switch_lane_called = False
        self.switch_to_lane     = 0

        self.turbo_available       = False
        self.turboing              = 0
        self.turbo_duration_msec   = 0
        self.turbo_duration_ticks  = 0
        self.turbo_factor          = 0

    def get_optimal_lane(self, currPos):

        # left turn
        if ( self.track.dir_of_next_turn_piece( currPos ) < 0 ): 
            return 0
        else:
            return len(self.track.lanes) - 1

    def dir_to_switch_lanes(self, currPos):

        position     = currPos['piecePosition']
        optimal_lane = self.get_optimal_lane( currPos )

        if ( position['lane']['startLaneIndex'] < optimal_lane ):
            return "Right"
        elif ( position['lane']['startLaneIndex'] > optimal_lane ):
            return "Left"
        else:
            return None

    def switch_lanes(self, direction):
        self.msg("switchLane", direction)

    def handle_turn_braking(self):
        if self.track.pieces[self.currPos['piecePosition']['pieceIndex'] + 1].get('angle'):
            return abs(self.currPos['angle']) > 15
        else:
            return abs(self.currPos['angle']) > 30

    def need_to_brake(self, data, game_tick):
        speed  = self.gps.get_current_speed(self.prevPos['piecePosition'], self.currPos['piecePosition'], 1)

        if self.track.on_curve( self.currPos['piecePosition'] ):
            return self.handle_turn_braking()
        else:
            dist_left, radius = self.track.dist_and_radius_of_next_turn(self.currPos['piecePosition'])
            dist_needed       = (speed - math.sqrt(radius * self.turn_friction)) / abs(self.deceleration_exponent)

            return dist_left <= dist_needed * 0.95

    def get_optimal_lane(self, currPos):

        # left turn
        if ( self.track.dir_of_next_turn_piece( currPos ) < 0 ): 
            return 0
        else:
            return len(self.track.lanes) - 1

    def dir_to_switch_lanes(self, currPos):

        position     = currPos['piecePosition']
        optimal_lane = self.get_optimal_lane( currPos )

        if ( position['lane']['startLaneIndex'] < optimal_lane ):
            return "Right"
        elif ( position['lane']['startLaneIndex'] > optimal_lane ):
            return "Left"
        else:
            return None

    def on_car_positions(self, data, game_tick = 0):
        for car in data:
            if car['id']['name'] == self.name:
                me = car

        self.prevPos = self.currPos
        self.currPos = me

        currentLane = me['piecePosition']['lane']['startLaneIndex']

        # ----------------
        # (1) Lane Control
        # ----------------
        if self.switch_lane_called == False:

            switch_lanes = self.dir_to_switch_lanes(self.currPos)

            if switch_lanes == "Right":
                self.switch_lanes( "Right" )
                self.switch_lane_called = True
                self.switch_to_lane = currentLane + 1
                return
            elif switch_lanes == "Left":
                self.switch_lanes( "Left" )
                self.switch_lane_called = True
                self.switch_to_lane = currentLane - 1
                return

        if self.switch_to_lane == currentLane:
            self.switch_lane_called = False

        # -----------------
        # (2) Turbo Control
        # -----------------

        if self.turbo_available == True:

            # if straight is longer than a certain length and turbo availiable, use it!
            if self.track.get_length_of_next_straight( self.currPos ) >= 400:
                self.turbo("pew pew")
                turbo_available = False

        # --------------------
        # (2) Throttle Control
        # -------------------- 
        if self.need_to_brake(data, game_tick):
            self.throttle(0.0)
        else:
            if ( self.turboing == True ):
                self.throttle(0.5)
            else:
                self.throttle(1)
