import pprint
import math

# internal imports
import track_info

# ================
# ===== GPS  =====
# ================
class Gps(object):

    def __init__(self, trackInfo):
        self.trackInfo = trackInfo
        return

    # Get Current Speed
    #   Using staring position and end position, we can figure out the current speed
    def get_current_speed(self, startPos, endPos, ticksBetween):
        distance = self.trackInfo.dist_between( startPos, endPos )
        return distance / ticksBetween

    def get_current_angle_change(self, startAngle, endAngle):
        return endAngle - startAngle